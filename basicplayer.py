from util import memoize, run_search_function

import connectfour


def basic_evaluate(board):
    """
    The original focused-evaluate function from the lab.
    The original is kept because the lab expects the code in the lab to be modified. 
    """
    if board.is_game_over():
        # If the game has been won, we know that it must have been
        # won or ended by the previous move.
        # The previous move was made by our opponent.
        # Therefore, we can't have won, so return -1000.
        # (note that this causes a tie to be treated like a loss)
        score = -1000
    else:
        score = board.longest_chain(board.get_current_player_id()) * 10
        # Prefer having your pieces in the center of the board.
        for row in range(6):
            for col in range(7):
                if board.get_cell(row, col) == board.get_current_player_id():
                    score -= abs(3-col)
                elif board.get_cell(row, col) == board.get_other_player_id():
                    score += abs(3-col)

    return score


def get_all_next_moves(board):
    """ Return a generator of all moves that the current player could take from this position """
    from connectfour import InvalidMoveException

    for i in xrange(board.board_width):
        if board.get_current_player_id() == 1:
            connectfour.nodes_expanded += 1
            # increment_nodes_expanded()
        try:
            yield (i, board.do_move(i))
        except InvalidMoveException:
            pass

def is_terminal(depth, board):
    """
    Generic terminal state check, true when maximum depth is reached or
    the game has ended.
    """
    return depth <= 0 or board.is_game_over()


def minimax(board, depth, eval_fn = basic_evaluate,
            get_next_moves_fn = get_all_next_moves,
            is_terminal_fn = is_terminal,
            verbose = True,max_turn=True):
    """
    Do a minimax search to the specified depth on the specified board.

    board -- the ConnectFourBoard instance to evaluate
    depth -- the depth of the search tree (measured in maximum distance from a leaf to the root)
    eval_fn -- (optional) the evaluation function to use to give a value to a leaf of the tree; see "focused_evaluate" in the lab for an example

    Returns an integer, the column number of the column that the search determines you should add a token to
    Returning the First Element of the tuple of minimax_custom output which is a column no
    """
    return minimax_custom(board, depth, True, eval_fn)[0]

def minimax_custom(board, depth, max_turn, eval_fn = basic_evaluate,
            get_next_moves_fn = get_all_next_moves,
            is_terminal_fn = is_terminal,
            verbose = True,):

    "If it's a terminal node, we return -1 as the chosen column"
    " basic_evaluate "
    "If max_turn's turn it get's positive score"
    if is_terminal(depth,board):
        if max_turn:
            return (-1,eval_fn(board))
        else:
            return (-1,-eval_fn(board))
    else:
        """"
        The best move for turn
        """""
        best_move=None
        for move in get_next_moves_fn(board):
            """""Evaluating the Current Move"""
            cur=minimax_custom(move[1],depth-1,not max_turn)
            if best_move is not None:
                """"
                If Part Max's turn or Part Min's Turn
                best_move(chosen_column,evaluated_value)
                """""
                if (max_turn and cur[1] > best_move[1]) or ( not max_turn and cur[1] < best_move[1]):
                    best_move=(move[0],cur[1])
            else:
                """
                best_move=first available move
                """
                best_move=cur
        return best_move

 
def rand_select(board):
    """
    Pick a column by random
    """
    import random
    moves = [move for move, new_board in get_all_next_moves(board)]
    return moves[random.randint(0, len(moves) - 1)]


def new_evaluate(board):
    if board.is_game_over():
        score = -1000
    else:
        score = board.longest_chain(board.get_current_player_id()) * 10
        score -= board.longest_chain(board.get_other_player_id()) * 10
        for row in range(6):
            for col in range(7):
                if board.get_cell(row, col) == board.get_current_player_id():
                    score -= abs(3-col) + abs(3-row)
                elif board.get_cell(row, col) == board.get_other_player_id():
                    score += abs(3-col) + abs(3-row)

    score += better_new_evaluate(board)
    return score


def better_new_evaluate(board):
    # return winning_possibility_score(board)
    # return weighted_heuristic_score(board, [1,-1, 0, 0])
    # return weighted_heuristic_score(board, [0.1,-0.1, 1, -1]) # Seems to be good
    return weighted_heuristic_score(board, [0,0, -1, +1])    # seems to be running bad

def longest_streak_evealuate_fn(board):
    return weighted_heuristic_score(board, [-1,+1, 0, 0])

def weighted_heuristic_score(board, mask):

    heuristic_scores = 4*[0]
    score = 0

    if board.is_game_over():
        score = -1000
    else:

        for row in range(6):

            wr_flag1 = False
            wr_flag2 = False

            for col in range(7):

                heuristic_scores[2] += board.is_threat(row, col, board.get_current_player_id())
                heuristic_scores[3] += board.is_threat(row, col, board.get_other_player_id())

                if board.get_cell(row, col) == board.get_current_player_id():
                    wr_flag1 = True
                elif board.get_cell(row, col) == board.get_other_player_id():
                    wr_flag2 = True
            
            if wr_flag1 and wr_flag2:
                continue
            if wr_flag1:
                heuristic_scores[0] += 1
            if wr_flag2:
                heuristic_scores[1] -= 1

    for i in range(0,4):
        score += mask[i] * heuristic_scores[i] * 50

    return score


def winning_possibility_score(board):
    possibility_score_matrix = [[3,4,5,7,5,4,3],
                                [4,6,8,10,8,6,4],
                                [5,8,11,13,11,8,5],
                                [5,8,11,13,11,8,5],
                                [4,6,8,10,8,6,4],
                                [3,4,5,7,5,4,3]]

    if board.is_game_over():
        score = -1000
    else:
        score = board.longest_chain(board.get_current_player_id()) * 10
        for row in range(6):
            for col in range(7):
                if board.get_cell(row, col) == board.get_current_player_id():
                    score -= possibility_score_matrix[row][col]
                elif board.get_cell(row, col) == board.get_other_player_id():
                    score += possibility_score_matrix[row][col]
    return score


random_player = lambda board: rand_select(board)
basic_player = lambda board: minimax(board, depth=4, eval_fn=basic_evaluate)
# new_player = lambda board: alpha_beta_search(board, depth=4, eval_fn=new_evaluate)
progressive_deepening_player = lambda board: run_search_function(board, search_fn=minimax, eval_fn=basic_evaluate)
